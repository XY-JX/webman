<?php

namespace App\bootstrap;

use Webman\Bootstrap;
use xy_jx\Utils\DelayQueue;

class MemReport implements Bootstrap
{
    public static function start($worker)
    {
        // 是否是命令行环境 ?
        $is_console = !$worker;
        if ($is_console) {
            // 如果你不想命令行环境执行这个初始化，则在这里直接返回
            return;
        }
        // monitor进程不执行定时器
        if ($worker->name == 'monitor') {
            return;
        }
        // 每隔10秒执行一次
        if ($worker->id == 0) {
            \Workerman\Timer::add(1, function () {
                // 为了方便演示，这里使用输出代替上报过程
                $redis = new DelayQueue(config('redis.default'));
                $redis->getdata(5);
            });
        }
    }
}
<?php

namespace App\middleware;

use Webman\Http\Request;
use Webman\Http\Response;
use Webman\MiddlewareInterface;

class Auth implements MiddlewareInterface
{
    public function process(Request $request, callable $handler): Response
    {
        // Access to files beginning with. Is prohibited
        if ($request->method() == 'PUT') {
            \Api::fail('404');
        }
        $route = $request->route; // 等价与 $route = request()->route;
        if ($route) {
            echo $route->getPath();echo PHP_EOL;
//            var_export($route->getMethods());echo PHP_EOL;
//            var_export($route->getName());echo PHP_EOL;
//            var_export($route->getMiddleware());echo PHP_EOL;
//            var_export($route->getCallback());echo PHP_EOL;
//            var_export($route->param()); // 此特性需要 webman-framework >= 1.3.16
        }
        $request->admin_id =$request->get('id');
        return $handler($request);
    }
}
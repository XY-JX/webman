<?php

namespace App;
abstract class BaseController
{

    protected function request()
    {
        return \Webman\App::Request();
    }

    protected function worker(): ?\Workerman\Worker
    {
        return \Webman\App::worker();
    }

    /**
     * 验证数据
     * @param array $data 需要验证的数据 ['api'=>'api','name'=>'name']
     * @param array $validate 验证规则数组  ['api|API'=>'require|email'] 参考tp6手册验证器
     * @param array $message 提示信息 ['name.require' => '名称必须','name.max' => '名称最多不能超过25个字符','email' => '邮箱格式错误'];
     */
    protected function validate(array $data, array $validate, array $message = [])
    {
        $v = new \think\Validate();

        $v->rule($validate);

        $v->setTypeMsg(config('validate_msg')['zh_CN']);

        $v->message($message);

        $v->failException(false)->check($data);

        if ($v->getError()) \Api::fail($v->getError(), 402);
    }

    /**
     *  获取请求数据并（过滤+默认值+验证）
     * @param array $params 数据 [['参数','默认值'],'api','name','id'=>100]
     * @param array $validate 验证规则数组  ['api|API'=>'require|email'] 参考tp6手册验证器
     * @param array $message 提示信息 ['name.require' => '名称必须','name.max' => '名称最多不能超过25个字符','email' => '邮箱格式错误'];
     * @return array|\support\Response|null
     */
    protected function getParam(array $params, array $validate = [], array $message = [])
    {

        $all = $this->request()->all();
        $p = [];
        foreach ($params as $key => $param) {
            if (is_array($param)) {
                $p[$param[0]] = $all[$param[0]] ?? ($param[1] ?? null);
            } else if (is_int($key)) {
                $p[$param] = $all[$param] ?? null;
            } else {
                $p[$key] = $all[$key] ?? $param;
            }
        }
        //如果有验证器
        if ($validate) $this->validate($p, $validate, $message);

        return $p;
    }


    /**
     * 方法不存在
     * @param $method
     * @param $args
     */
    public function __call($method, $args)
    {
        \Api::fail('资源不存在', 410);
    }

}
<?php

namespace App\logic;

use App\model\luck_draw\LuckyDrawLog;

class ApiLogic
{

    public function info($param)
    {
        return LuckyDrawLog::find($param['id']);
    }

    public function all()
    {
        return LuckyDrawLog::order('id', 'desc')->select();
    }
}
<?php

namespace App\controller\api;

use App\logic\ApiLogic;
use xy_jx\Utils\Sundry;

class Api extends \App\BaseController
{

    public function index()
    {
        $data = $this->getParam(['username' => 1, 'api'], ['api|API' => 'require']);
        \Api::success($data);
    }

    public function info(ApiLogic $logic)
    {
        $param = $this->getParam(['id'], ['id' => 'require|number']);
        \Api::success($logic->info($param));
    }

    public function redis()
    {
        $redis = Sundry::redis(config('redis.default'));
        $key = 'test_key2';
        $redis->set($key, mt_rand(5, 9));
        \Api::success($redis->get($key));
    }

    public function cs()
    {
        \Api::success($this->request()->admin_id);
    }


    public function code()
    {
        $builder = new \xy_jx\Utils\DelayQueue(config('redis.default'));

        // 将验证码的值存储到session中
        \Api::success($builder->add(['a' => 'b'], 1));
        // 获得验证码图片二进制数据

        \Api::success($builder->create());
        // 输出验证码二进制数据
    }

    public function all(ApiLogic $logic)
    {
        \Api::success($logic->all());
    }

    public function workers()
    {
        \Api::success($this->worker());
    }
}
# webman

# 二次封装

Api 统一返回  (已接管系统错误)

$this->getParam() 更友好的获取请求数据并验证  （需要继承Base）

### 特殊说明

调试模式才会返回错误信息（非调试模式，错误信息只能在日志中查看）

# 运行

进入webman目录

## 启动
以debug（调试）方式启动

php start.php start

以daemon（守护进程）方式启动

php start.php start -d

## 停止
php start.php stop

## 重启
php start.php restart

## 平滑重启
php start.php reload

## 查看状态
php start.php status

## 查看连接状态（需要Workerman版本>=3.5.0）
php start.php connections

## windows用户用 
双击windows.bat 或者运行 php windows.php 启动

# 访问

浏览器访问 http://ip地址:8787

# 关于输出语句
在传统php-fpm项目里，使用echo var_dump等函数输出数据会直接显示在页面里，而在webman中，这些输出往往显示在终端上，并不会显示在页面中(模版文件中的输出除外)。下面提供了更好的输出方式:
```
\Api::success();   //使用该方法后不在执行后面的代码，直接返回数据 (可在任意地方调用)
\Api::fail();      //使用该方法后不在执行后面的代码，直接返回数据 (可在任意地方调用)
```

# 不要执行exit die语句
执行die或者exit会使得进程退出并重启，导致当前请求无法被正确响应。

# 不要执行pcntl_fork函数
pcntl_fork用户创建一个进程，这在webman中是不允许的。




<?php
/**
 * This file is part of webman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link      http://www.workerman.net/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Webman\Route;

Route::group('/v1/', function () {
    Route::get('index', [\App\controller\api\Api::class, 'index']);
    Route::get('info', [\App\controller\api\Api::class, 'info']);
    Route::get('redis', [\App\controller\api\Api::class, 'redis']);
    Route::get('cs', [\App\controller\api\Api::class, 'cs']);
    Route::get('code', [\App\controller\api\Api::class, 'code']);
    Route::get('all', [\App\controller\api\Api::class, 'all']);
    Route::get('worker',[\App\controller\api\Api::class,'workers']);
})->middleware([app\middleware\Auth::class]);
Route::get('/', function () {
    \Api::success('webman');
});
Route::fallback(function () {//无匹配路由兜底
    \Api::fail('api', 405);
});

Route::disableDefaultRoute();//关闭默认路由


